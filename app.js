class Hamburger {
    constructor(size, stuffing) {

        this.size = size;

        this.stuffing = stuffing;

        this.toppings = [];

    }

    static SIZE_SMALL = {price: 50, calories: 20};

    static SIZE_LARGE = {price: 100, calories: 40};

    static STUFFING_CHEESE = {price: 10, calories: 20};

    static STUFFING_SALAD = {price: 20, calories: 5};

    static STUFFING_POTATO = {price: 15, calories: 10};

    static TOPPING_SPICE = {price: 15, calories: 0};

    static TOPPING_MAYO = {price: 20, calories: 5};


    addTopping(topping) {
        this.toppings.push(topping);
    }

    calculatePrice() {

        let price = 0;

        price += this.size.price;

        this.stuffing?.price ? price += this.stuffing.price : 0;

        this.toppings.forEach((topping) => {
            price += topping.price;
        });
        return price;
    }

    calculateCalories() {

        let calories = 0;
        calories += this.size.calories;

        this.stuffing?.calories ? calories += this.stuffing.calories : 0;

        this.toppings.forEach((topping) => {
            calories += topping.calories;
        });
        return calories;
    }

}


const hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);

hamburger.addTopping(Hamburger.TOPPING_MAYO);


const price = hamburger.calculatePrice();
const calories = hamburger.calculateCalories();

console.log(price);
console.log(calories);

//Hamburger.SIZE_SMALL
//Hamburger.SIZE_LARGE
//Hamburger.STUFFING_CHEESE
//Hamburger.STUFFING_SALAD
//Hamburger.STUFFING_POTATO
//Hamburger.TOPPING_SPICE
//Hamburger.TOPPING_MAYO